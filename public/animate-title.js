var heads = [
    '👩',
    '👩🏻',
    '👩🏽',
    '👩🏾',
    '👩🏿',
    '👨',
    '👨🏻',
    '👨🏼',
    '👨🏽',
    '👨🏾',
    '👨🏿',
    '👵',
    '👵🏻',
    '👵🏼',
    '👵🏽',
    '👵🏾',
    '👵🏿',
    '👴',
    '👴🏻',
    '👴🏼',
    '👴🏽',
    '👴🏾',
    '👴🏿',
    '👶',
    '👶🏻',
    '👶🏼',
    '👶🏼',
    '👶🏽',
    '👶🏾',
    '👶🏿',
    '👦🏽',
    '👦',
    '👦🏻',
    '👦🏼',
    '👦🏾',
    '👦🏿',
    '👧',
    '👧🏻',
    '👧🏼',
    '👧🏽',
    '👧🏾',
    '👧🏿',
    '🧕',
    '🧕🏻',
    '🧕🏼',
    '🧕🏽',
    '🧕🏾',
    '🧕🏿',
    '🤖',
    '😸',
    '🧓',
    '🧔🏻',
    '🧔🏼',
    '🧔🏾',
    '🧔🏿',
    '🐶',
];

// Create canvas element with correct size for favicon.
const canvas = document.createElement('canvas');
canvas.width = 16;
canvas.height = 16;

// Get context and set font
const context = canvas.getContext('2d');
context.font = '13px serif';

// Grab favicon element.
const favicon = document.getElementById('kop-favicon');

setInterval(() => {
    // set a new random head every few seconds
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.fillText(heads[Math.floor(Math.random() * heads.length)], 0, 13);
    favicon.href = canvas.toDataURL();
}, 5000);
