const form = document.getElementById('form');
var input = document.querySelector('input[type="file"]');

form.addEventListener('submit', e => {
    e.preventDefault();

    var data = new FormData();
    data.append('image', input.files[0]);

    fetch('http://localhost:3000/image', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            //'Content-Type': 'multipart/form-data',
        },
        body: data,
    })
        .then(
            response => response.json(), // if the response is a JSON object
        )
        .then(
            success => console.log(success), // Handle the success response object
        )
        .catch(
            error => console.log(error), // Handle the error response object
        );
});
